#########################################################################
# File Name: ctl_com_mysql.sh
# Author: Robin Luo
# Mail: robin.luo@beyondsoft.com
# Created Time: Thu 23 Jun 2016 03:32:24 AM UTC
#########################################################################
#!/bin/bash

# Change it if you have use another different docker registry
DOCKER_URL="bys-cd.chinacloudapp.cn"

data_dir="/opt/beyondsoft/mysql/db-slave"
if [ ! -d ${data_dir} ]; then
  mkdir -p ${data_dir}
  if [ $? -ne 0 ]; then
    sudo mkdir -p ${data_dir}
  fi
  sudo chmod 0777 ${data_dir}
fi

conf=$(cd "$(dirname "$0")"; pwd)/my_slave.cnf
if [ ! -f ${conf} ]; then
  echo "${conf} not exist!!!"
  exit -1
fi
chmod 0644 ${conf}

name=c-mysql-slave
echo "stop previous container - ${name}"
docker stop ${name} && docker rm -v ${name}

echo "update latest image - ${name}"
docker pull ${DOCKER_URL}/library/mysql:5.7

db_name="fnb_sell"
db_user="bys"
db_pswd="bys2018"

status="done"
#docker run -d --restart=always \
docker run --rm -it \
  --name ${name} \
  -e "MYSQL_DATABASE=${db_name}" \
  -e "MYSQL_ROOT_PASSWORD=${db_pswd}" \
  -e "MYSQL_USER=${db_user}" \
  -e "MYSQL_PASSWORD=${db_pswd}" \
  -p 3307:3306 \
  -v ${data_dir}:/var/lib/mysql \
  -v ${conf}:/etc/mysql/my.cnf:ro \
  ${DOCKER_URL}/library/mysql:5.7 --verbose 

if [ $? -ne 0 ]; then
  status="failed"
fi
echo -e ">>> [DONE] start container - ${name} ${status}\n"
