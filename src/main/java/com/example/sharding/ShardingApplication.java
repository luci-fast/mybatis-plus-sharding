package com.example.sharding;

import org.apache.shardingsphere.transaction.xa.XAShardingTransactionManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement(proxyTargetClass = true)
@SpringBootApplication
public class ShardingApplication {

//    @Bean
//    public XAShardingTransactionManager transactionManager(){
//        XAShardingTransactionManager xa = new XAShardingTransactionManager();
//        return xa;
//    }

    public static void main(String[] args) {
        SpringApplication.run(ShardingApplication.class, args);
    }

}
