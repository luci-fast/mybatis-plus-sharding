package com.example.sharding.t.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * <p>
 *   字典表
 * </p>
 *
 * @author yuyanan
 * @since 2019-06-01
 */
@Data
@TableName("o_dict")
public class Dict {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    private Long id;

    private Long shopId;

    private String name;
}
