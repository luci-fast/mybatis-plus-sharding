package com.example.sharding.t.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("o_order_child")
public class OrderChild {

    @TableId(type = IdType.INPUT)
    private Long id;

    private Long orderId;

    private String name;

}
