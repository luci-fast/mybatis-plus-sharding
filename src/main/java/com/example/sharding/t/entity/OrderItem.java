package com.example.sharding.t.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author yuyanan
 * @since 2019-06-02
 */
@Data
@TableName("o_order_item")
public class OrderItem {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    private Long id;

    private String name;

    private Long orderId;

    private Long shopId;


}
