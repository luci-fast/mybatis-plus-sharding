package com.example.sharding.t.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.sharding.t.entity.Dict;
import com.example.sharding.t.entity.OrderItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DictMapper  extends BaseMapper<Dict> {

    int createTableIfNotExists();
}
