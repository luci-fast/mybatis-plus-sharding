package com.example.sharding.t.mapper;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.sharding.t.entity.OrderChild;
import lombok.Data;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface OrderChildMapper extends BaseMapper<OrderChild> {

}
