package com.example.sharding.t.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.sharding.t.entity.OrderItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yuyanan
 * @since 2019-06-02
 */
@Mapper
public interface OrderItemMapper extends BaseMapper<OrderItem> {

    List<OrderItem> joinTest();

    int createTableIfNotExists();

}
