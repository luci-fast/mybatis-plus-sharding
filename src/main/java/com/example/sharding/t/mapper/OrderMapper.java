package com.example.sharding.t.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.sharding.t.entity.Order;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author yuyanan
 * @since 2019-06-01
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

    int getCount();

    void add();

    int createTableIfNotExists();
}
