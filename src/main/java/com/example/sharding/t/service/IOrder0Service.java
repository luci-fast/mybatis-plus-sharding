package com.example.sharding.t.service;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author yuyanan
 * @since 2019-06-01
 */
public interface IOrder0Service {
    public Object getList();

    void batchAdd();
}
