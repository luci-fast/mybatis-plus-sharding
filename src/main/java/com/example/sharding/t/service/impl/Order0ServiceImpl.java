package com.example.sharding.t.service.impl;

import com.example.sharding.t.entity.Order;
import com.example.sharding.t.entity.OrderItem;
import com.example.sharding.t.mapper.OrderItemMapper;
import com.example.sharding.t.mapper.OrderMapper;
import com.example.sharding.t.service.IOrder0Service;
import org.apache.shardingsphere.transaction.core.TransactionType;
import org.apache.shardingsphere.transaction.core.TransactionTypeHolder;
import org.apache.shardingsphere.transaction.xa.XAShardingTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yuyanan
 * @since 2019-06-01
 */
@Service
public class Order0ServiceImpl implements IOrder0Service {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderItemMapper itemMapper;

    @Override
    public Object getList() {
        return orderMapper.selectList(null);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchAdd() {
        TransactionTypeHolder.set(TransactionType.XA);
        for (int i=41;i<=60; i++) {
            Order order  = new Order();
            order.setId(new Long(i));
            order.setShopId(new Long(i));
            order.setCode(i + "test");
            orderMapper.insert(order);
            for(int j=1;j<=10; j++) {
                OrderItem orderItem = new OrderItem();
                orderItem.setId(new Long((i*10)+j));
                orderItem.setOrderId(order.getId());
                orderItem.setName("明细 "+ j);
                orderItem.setShopId(new Long(i));
                itemMapper.insert(orderItem);
            }
        }
    }
}
