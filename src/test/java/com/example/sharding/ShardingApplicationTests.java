package com.example.sharding;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.sharding.t.entity.Dict;
import com.example.sharding.t.entity.Order;
import com.example.sharding.t.entity.OrderChild;
import com.example.sharding.t.entity.OrderItem;
import com.example.sharding.t.mapper.DictMapper;
import com.example.sharding.t.mapper.OrderChildMapper;
import com.example.sharding.t.mapper.OrderItemMapper;
import com.example.sharding.t.mapper.OrderMapper;
import com.example.sharding.t.service.IOrder0Service;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableTransactionManagement(proxyTargetClass = true)

public class ShardingApplicationTests {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private IOrder0Service order0Service;

    @Autowired
    private OrderItemMapper itemMapper;

    @Autowired
    private DictMapper dictMapper;

    @Autowired
    private OrderChildMapper childMapper;

    @Test
    public void insert() throws SQLException {
        order0Service.batchAdd();
    }

    @Test
    public void get(){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("id", 413);
        queryWrapper.eq("order_id", 41);
        itemMapper.selectOne(queryWrapper);
    }

    @Test
    public void get2(){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("id", new Long(343410362634207233L));
        queryWrapper.eq("shop_id", new Long(41L));
        Order orderItem = orderMapper.selectOne(queryWrapper);
        System.out.println(JSON.toJSONString(orderItem));
    }

    @Test
    public void join(){
        List<OrderItem> list = itemMapper.joinTest();
        System.out.println(JSON.toJSONString(list));
    }

    @Test
    public void orderCount(){
        int count = orderMapper.getCount();
        System.out.println(JSON.toJSONString(count));
    }
    @Test
    public void add(){
      orderMapper.add();
    }

    @Test
    public void create(){
        orderMapper.createTableIfNotExists();
        itemMapper.createTableIfNotExists();
    }

    @Test
    public void insertOrder() throws InterruptedException {
        for(int i=0;i<20;i++) {
            Order order  = new Order();
            order.setShopId(new Long(1));
            order.setCode(1 + "test");
            order.setCreateTime(new Date());
            orderMapper.insert(order);
            Thread.sleep(1000);
        }
    }

    @Test
    public void createDict() throws InterruptedException {
        dictMapper.createTableIfNotExists();
    }

    @Test
    public void test() throws InterruptedException {
        System.out.println(JSON.toJSONString(childMapper.selectById(999)));
    }

    @Test
    public void page() {
        orderMapper.selectPage(new Page<>(1, 10),null);
    }
}
